const EventEmitter = require('events').EventEmitter;
const sinon = require('sinon');
const fs = require('fs');

describe('Given the LogsController class', () => {
	let sandbox;

	beforeEach( () => {
		sandbox = sinon.sandbox.create();
	});

	afterEach(() => {
		sandbox.restore();
	});

	describe('when required', () => {
        let LogsController;

        beforeEach(() => {
            LogsController = require('./../../lib/logs-controller');
        });

        it('should be a function', () => {
            LogsController.should.be.a('function');
        });

        describe('and instantiated', () => {
            let logsController, logfile;

            beforeEach(() => {
                logfile = 'some/file/path';

                logsController = new LogsController(logfile);
            });

            it('should be an instance of LogsController', () => {
                logsController.should.be.an.instanceOf(LogsController);
            });

            it('should extend the EventEmitter', () => {
                logsController.should.be.an.instanceOf(EventEmitter);
            });

            describe('and a message is logged', () => {
                let msg, resolveAppendFile;

                beforeEach(() => {
                    msg = 'bogus message';

                    sandbox.stub(fs, 'appendFile').callsFake((file, data, callback) => {
                        resolveAppendFile = callback;
                    });

                    logsController.log(msg);
                });

                it('should append the message to the logfile', () => {
                    fs.appendFile.withArgs(logfile, msg).should.have.been.calledOnce;
                });

                describe('and is resolved without an error', () => {
                    beforeEach(() => {
                        sandbox.stub(process.stdout, 'write');

                        resolveAppendFile();
                    });

                    it('should not output anything to stdout', () =>{
                        process.stdout.write.should.not.have.been.called;
                    });
                });

                describe('and is resolved with an error', () => {
                    let error;

                    beforeEach(() => {
                        error = new Error('bogus error');

                        sandbox.stub(process.stdout, 'write');

                        resolveAppendFile(error);
                    });

                    it('should output the error to stdout', () => {
                        process.stdout.write.withArgs(error).should.have.been.calledOnce;
                    });
                });
            });

            describe('and multiple messages are logged', () => {
                let firstMsg, secondMgs, thierdMgs, callbacks;

                beforeEach(() => {
                    firstMsg = 'first message';
                    secondMgs = 'second message';
                    thierdMgs = 'thierd message';
                    callbacks = [];

                    sandbox.stub(fs, 'appendFile').callsFake((file, data, callback) => {
                        callbacks.push(callback);
                    });

                    logsController.log(firstMsg);
                    logsController.log(secondMgs);
                    logsController.log(thierdMgs);
                });

                it('should only try to append once before its resolved', () => {
                    fs.appendFile.should.have.been.calledOnce;
                });

                function resolveNext() {
                    const callback = callbacks.shift();

                    callback();
                }

                describe('and appending the first message is resolved', () => {
                    beforeEach(() => {
                        resolveNext();
                    });

                    it('should try to append a second time', () => {
                        fs.appendFile.should.have.been.calledTwice;
                    });

                    describe('and appending the second message is resolved', () => {
                        beforeEach(() => {
                            resolveNext();
                        });

                        it('should try to append with the third message', () => {
                            fs.appendFile.withArgs(logfile, thierdMgs).should.have.been.calledOnce;
                        });
                    });
                });
            });
        });
    });
});