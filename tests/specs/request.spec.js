const EventEmitter = require('events').EventEmitter;
const sinon = require('sinon');

describe('Given the Request class', function () {
    let sandbox;

    beforeEach( () => {
        sandbox = sinon.sandbox.create();
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe('when required', () => {
        let Request;

        beforeEach(() => {
            Request = require('./../../lib/request');
        });

        it('should be a function', () => {
            Request.should.be.a('function');
        });

        describe('and when instantiated', () => {
            let request, mockedSocket;

            beforeEach(() => {
                mockedSocket = new EventEmitter();

                request = new Request(mockedSocket);
            });

            it('should be an instance of the Request class', () => {
                request.should.be.an.instanceOf(Request);
            });

            it('should extend the EventEmitter class', () => {
                request.should.be.an.instanceOf(EventEmitter);
            });

            describe('and when the socket emits a error event', () => {
                let mockedError, spy;

                beforeEach(() => {
                    mockedError = Error('bogus error');
                    spy = sandbox.spy(request, 'emit');
                    request.on('error', () => null);

                    mockedSocket.emit('error', mockedError);
                });

                it('should emit the same error', () => {
                    spy.withArgs('error', mockedError).should.have.been.calledOnce
                });
            });

            describe('and when the socket emits multiple data events', () => {
                let firstChunk, secondChunk;

                beforeEach(() => {
                    firstChunk = 'foo';
                    secondChunk = 'bar';

                    mockedSocket.emit('data', new Buffer(firstChunk));
                    mockedSocket.emit('data', new Buffer(secondChunk));
                });

                describe('and then the socket emits a close event', () => {
                    let spy, mockedData;

                    beforeEach(() => {
                        mockedData = firstChunk + secondChunk;

                        spy = sandbox.spy(request, 'emit');

                        mockedSocket.emit('close');
                    });

                    it('should emit a data event with the expected data', function () {
                       spy.withArgs('data', mockedData).should.have.been.calledOnce;
                    });
                });
            });
        });
    });
});