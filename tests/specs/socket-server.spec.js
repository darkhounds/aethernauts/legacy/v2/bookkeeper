const EventEmitter = require('events').EventEmitter;
const sinon = require('sinon');
const net = require('net');

describe('Given the SocketServer class', () => {
	let sandbox;

	beforeEach( () => {
		sandbox = sinon.sandbox.create();
	});

	afterEach(() => {
		sandbox.restore();
	});

	describe('when required', () => {
		let SocketServer, mockedServer, port, address;

		beforeEach(() => {
			port = 9999;
			address = {
				port: port
			};
			mockedServer = new EventEmitter();
			mockedServer.address = () => address;

			sandbox.stub(net, 'createServer').callsFake(callback => {
				mockedServer.resolveNewSocket = callback;

				return mockedServer;
			});

			SocketServer = require('./../../lib/socket-server.js');
		});

		it('should be a function', () => {
			SocketServer.should.be.a('function');
		});

		describe('and instantiated', () => {
			let socketServer;

			beforeEach(() => {
				mockedServer.listen = sandbox.stub().callsFake((port, callback) => {
					mockedServer.resolveListen = callback;
				});

				socketServer = new SocketServer(port);

				socketServer.on('error', () => null);
			});

			it('should be an instance of SocketServer', () => {
				socketServer.should.be.an.instanceOf(SocketServer);
			});

			it('should extend the EventEmitter', () => {
				socketServer.should.be.an.instanceOf(EventEmitter);
			});

			it('should invoke the listen method of the server with the expected port', () => {
				mockedServer.listen.withArgs(port).should.have.been.calledOnce;
			});

			describe('and the server resolves the listen method', () => {
				var spy;

				beforeEach(() => {
					spy = sandbox.spy(socketServer, 'emit');

					mockedServer.resolveListen();
				});

				it('should emit the started event with the expected port', () => {
					spy.withArgs('started', port).should.have.been.calledOnce;
				});
			});

			describe('and the server emits an error', () => {
				let expectedError, spy;

				beforeEach(() => {
					expectedError = new Error('bogus error');

					spy = sandbox.spy(socketServer, 'emit');

					mockedServer.emit('error', expectedError)
				});

				it('should emit the error event with the expected error', () => {
					spy.withArgs('error', expectedError).should.have.been.calledOnce;
				});
			});

			describe('and a new socket is created', () => {
				let mockedSocket;

				beforeEach(() => {
					mockedSocket = new EventEmitter();

					mockedServer.resolveNewSocket(mockedSocket);
				});

				describe('and the socket emits a close event', () => {
					let expectedData, spy;

					beforeEach(() => {
						expectedData = 'foobar';

						spy = sandbox.spy(socketServer, 'emit');

						mockedSocket.emit('data', new Buffer(expectedData));
						mockedSocket.emit('close');
					});

					it('should emit a request event with the expected data', () => {
						spy.withArgs('request', expectedData).should.have.been.calledOnce;
					})
				});

				describe('and the socket emits an error event', () => {
					let expectedError, spy;

					beforeEach(() => {
						expectedError = new Error('bogus error');

						spy = sandbox.spy(socketServer, 'emit');

						mockedSocket.emit('error', expectedError);
					});

					it('should emit an error event with expected error', () => {
						spy.withArgs('error', expectedError).should.have.been.calledOnce;
					});
				});
			});
		});

		describe('and instantiated but the server fails to bind with the selected port', () => {
			var socketServer, expectedError, spy;

			beforeEach(() => {
				expectedError = new Error('bogus error');

				mockedServer.listen = sandbox.stub().callsFake(() => {
					throw expectedError;
				});

				socketServer = new SocketServer(port);

				spy = sandbox.spy(socketServer, 'emit');
			});

			it('should emit an error event with expected error when failing to call the server listen method', done => {
				socketServer.on('error', error => {
					error.should.equal(expectedError);

					done();
				})
			});
		});
	});
});