const net = require('net');
const EventEmitter = require('events').EventEmitter;
const util = require('util');
const Request = require('./request');

function SocketServer(port) {
	this._server = net.createServer(this._handleSocket.bind(this));
	this._server.on('error', this._handleError.bind(this));

	try {
		this._server.listen(port, this._handleStart.bind(this));
	} catch (error) {
		setTimeout(() => {
			this.emit('error', error);
		});
	}
}
util.inherits(SocketServer, EventEmitter);

SocketServer.prototype._handleStart = function () {
	const address = this._server.address();

	this.emit('started', address.port);
};

SocketServer.prototype._handleError = function (error) {
	this.emit('error', error)
};

SocketServer.prototype._handleSocket = function (socket) {
	const request = new Request(socket);

	request.on('data', this._handleRequestData.bind(this));
	request.on('error', this._handleRequestError.bind(this));
};

SocketServer.prototype._handleRequestData = function (data) {
	this.emit('request', data);
};

SocketServer.prototype._handleRequestError = function (error) {
	this.emit('error', error);
};

module.exports = SocketServer;
