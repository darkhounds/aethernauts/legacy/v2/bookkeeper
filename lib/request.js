const EventEmitter = require('events').EventEmitter;
const util = require('util');

function Request (socket) {
	this._data = new Buffer(0);

	socket.on('error', error => {
		this.emit('error', error);
	});

	socket.on('data', chunk => {
		this._data = Buffer.concat([this._data, chunk]);
	});

	socket.on('close', () => {
		const data = this._data.toString();

		this.emit('data', data);
	});
}
util.inherits(Request, EventEmitter);

module.exports = Request;