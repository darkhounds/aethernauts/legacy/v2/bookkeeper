const EventEmitter = require('events').EventEmitter;
const util = require('util');
const fs = require('fs');

function LogsController (logfile) {
    this._logfile = logfile;
    this._messages = [];
}
util.inherits(LogsController, EventEmitter);

LogsController.prototype.log = function (msg) {
    this._messages.push(msg);

    if (this._messages.length === 1) {
        this._logMessage();
    }
};

LogsController.prototype._logMessage = function () {
    if (this._messages.length) {
        const data = this._messages[0];

        fs.appendFile(this._logfile, data, error => {
            if (error) {
                process.stdout.write(error);
            }

            this._logNextMessage();
        });
    }
};

LogsController.prototype._logNextMessage = function () {
    this._messages.shift();

    this._logMessage();
};

module.exports = LogsController;