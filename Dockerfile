FROM alpine

RUN apk update
RUN apk add openrc
RUN apk add nodejs

ADD ./logs /root/logs

ADD ./lib /root/lib

ADD ./bin /root/bin
RUN chmod u+x /root/bin/*
ENV PATH="${PATH}:/root/bin"

ENTRYPOINT start-server 2>> /root/logs/system.log & ash
