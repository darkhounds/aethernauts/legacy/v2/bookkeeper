# Bookkeeper 

### This project aims to serve as a dockerised logging server for the Aethernauts applications.

Build the docker image by running:
```npm
npm run build
```

Start the container with the default settings by running:
```npm
npm run start
```

Stop the container with the default name by running:
```npm
npm run stop
```

Remove the container with the default name by running:
```npm
npm run remove
```

Stop, Remove and Start the container with the default name and settings by running:
```npm
npm run restart
```
